
from info import create_app, db
from utils import models
from flask import Flask, session, current_app
import logging
from flask_migrate import Migrate,MigrateCommand
from flask_script import Shell,Manager

app = create_app("develop")
# 创建migrate对象,关联app,db
Migrate(app,db)

# 创建Manager对象，关联app
manager = Manager(app)
# 给manager增加操作命令
manager.add_command("db", MigrateCommand)

if __name__ == '__main__':

    manager.run()