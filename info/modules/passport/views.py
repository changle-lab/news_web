from datetime import datetime
import random
import re

from flask import current_app, json, jsonify, session
from flask import make_response
from flask import request
from werkzeug.security import generate_password_hash, check_password_hash

from info.libs.yuntongxun.sms import CCP
from utils import constants
from info import redis_store, db

from utils.captcha.captcha import captcha
from utils.constants import SMS_CODE_REDIS_EXPIRES
from utils.models import User
from . import passport_blue
from utils.response_code import *


@passport_blue.route("/logout", methods=["post"])
def log_out():
    session.pop("user_id", None)
    session.pop("nick_name", None)

    return jsonify(errno=RET.OK, errmsg="退出成功")


# 登录
@passport_blue.route("/login", methods=["post"])
def login():
    # 获取数据
    data_json = request.data
    datas = json.loads(data_json)
    mobile = datas.get("mobile")
    password = datas.get("password")
    # 一起做为空校验
    if not all([mobile, password]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数不全")
    user = User.query.filter(User.nick_name == mobile).first()
    # 判断是否有这个账户
    if not user:
        return jsonify(error=RET.THIRDERR, errmsg="账户不存在")
    # 判断密码是否正确
    # if user.password_hash != password:
    if not check_password_hash(user.password_hash, password):
        return jsonify(error=RET.DBERR, errmsg="密码错误了")

    session["user_id"] = user.id
    session["nick_name"] = user.nick_name

    user.last_login = datetime.now()
    try:
        db.session.commit()

    except Exception as e:
        current_app.logger.error(e)

    return jsonify(errno=RET.OK, errmsg="登录成功")


# 注册接口实现
@passport_blue.route("/register", methods=["post"])
def register_index():
    # 获取参数
    data = request.data  # 获取json数据
    data = json.loads(data)  # 把json转成字典，取出接口文档里的参数
    mobile = data.get("mobile")
    print(mobile)
    sms_code = data.get("sms_code")
    print(sms_code)
    password = data.get("password")
    print(password)
    # 为空判断
    if not sms_code or not password or not mobile:
        return jsonify(errno=RET.DATAEXIST, errmsg="参数不能为空")

    # 手机好格式判断
    mobile_ret = re.match(r"^1[34567]\d{9}$", mobile)  # 匹配到为值，匹配不到返回None
    print(mobile_ret)
    if not mobile_ret:
        return jsonify(errno=RET.DBERR, errmsg="手机号格式不正确")
    # 密码格式判断
    # lowerRegex = re.compile('[a-z]')
    # upperRegex = re.compile('[A-Z]')
    # digitRegex = re.compile('[0-9]')
    # wrongRegex = re.compile('[^a-zA-Z0-9]')
    #
    # while True:
    #     if len(password) < 8:
    #         print('输入的密码小于8位')
    #         # 这里注意的是，只要errno返回的是“0”，那么前段就注册成功了，及时密码不符合规则，返回的参数绝对了很多东西
    #         return jsonify(errno=RET.DBERR, errmsg='输入的密码小于8位')
    #
    #     elif wrongRegex.search(password) != None:
    #         print('包含无效字符')
    #         return jsonify(errno=RET.DBERR, errmsg='包含无效字符')
    #     else:
    #         if lowerRegex.search(password) == None:
    #             print('未包含小写字母')
    #             return jsonify(errno=RET.DBERR, errmsg='未包含小写字母')
    #
    #         elif upperRegex.search(password) == None:
    #             print('未包含大写字母')
    #             return jsonify(errno=RET.DBERR, errmsg='未包含大写字母')
    #         elif digitRegex.search(password) == None:
    #             print('未包含数字')
    #             return jsonify(errno=RET.DBERR, errmsg="未包含数字")
    #         else:
    #             break

    # 取出redis短信验证码

    try:
        sms_mobile = redis_store.get("sms_%s" % mobile)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.THIRDERR, errmsg="获取redis中数据出现异常")

    # 和前段短信值做对比
    if sms_code != sms_mobile:
        return jsonify(error=RET.DATAERR, errmsg="短信验证码错误")

    # 把账号存到数据库，名为手机号，密码为password
    user = User()
    user.nick_name = "%s" % mobile
    user.password_hash = generate_password_hash(password)
    user.mobile = mobile
    try:
        db.session.add(user)
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.THIRDERR, errmsg="注册失败")

    return jsonify(errno=RET.OK, errmsg="注册成功")


# 发送短信验证码借口
@passport_blue.route("/sms_code", methods=["post"])
def mascode():
    # 获取参数
    msg = request.data
    sms_code = json.loads(msg)
    mobile = sms_code.get("mobile")
    print(mobile)
    image_code = sms_code.get("image_code")
    print(image_code)
    image_code_id = sms_code.get("image_code_id")
    print(image_code_id)
    # 验证参数
    if not image_code_id or not image_code:
        return jsonify(error=RET.DATAEXIST, errmsg="验证编码不能为空")
    ret = re.match(r"^1[35678]\d{9}$", mobile)
    if not ret:
        return jsonify(error=RET.DATAERR, errmsg="手机号不能为空")
    # 验证图片验证码是否正确
    try:
        image_code_pre = redis_store.get("image_code:%s" % image_code_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="图片验证码取出失败")

    if not image_code_pre:
        return jsonify(errno=RET.NODATA, errmsg="输入验证码已经过期")
    if image_code.lower() != image_code_pre.lower():
        return jsonify(errno=RET.DATAERR, errmsg="输入验证码错误")

    # 调用第三方云通讯发送短信
    sms = "%06d" % random.randint(0, 999999)

    print(sms)

    # try:
    #     ccp = CCP()
    #     result = ccp.send_template_sms(mobile, [sms, 3], 1)
    # except Exception as e:
    #     current_app.logger.error(e)
    #
    #     return jsonify(errno=RET.THIRDERR, errmsg="云通讯异常")
    #
    # if result == -1:
    #     return jsonify(errno=RET.DATAERR, errmsg="发送短信失败")

    # 把短信验证码存到redis中
    try:
        redis_store.set("sms_%s" % mobile, sms, 500)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.THIRDERR, errmsg="储存验证码失败")

    return jsonify(errno=RET.OK, errmsg="发送短信成功")


@passport_blue.route("/image_code")
def passPort():
    # 获取参数

    cur_id = request.args.get("cur_id")
    pre_id = request.args.get("pre_id")

    # 校验参数
    if not cur_id:
        return "图片验证吗不能为空"

    # 查看之前参数是否存在，存在就删除
    try:
        if pre_id:
            redis_store.delete("image_code:%s" % pre_id)
    except Exception as e:
        current_app.logger.error(e)

    # 把新生成的参数储存到redis中
    name, text, img = captcha.generate_captcha()
    try:
        redis_store.set("image_code:%s" % cur_id, text, constants.IMAGE_CODE_REDIS_EXPIRES)
    except Exception as e:
        current_app.logger.error(e)
        return "储存失败"

    # 返回图片

    response = make_response(img)
    response.headers["Content-Type"] = "image/png"

    return response
