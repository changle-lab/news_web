from flask import render_template, abort, current_app, jsonify, session, g, request, json

from info import db
from utils.commons import user_login_data
from utils.models import News, User, Comment, CommentLike
from utils.response_code import RET
from . import news_blue


#关注、取消关注
@news_blue.route("/followed_user", methods=["GET", "POST"])
@user_login_data
def followed_user():
    #判断用户是否登录
    if not g.user:
        return jsonify(errno=RET.NODATA, errmsg= "用户没有登录")
    #获取参数
    author_id = request.json.get("user_id")
    action = request.json.get("action")
    #校验参数
    if not all([author_id, action]):
        return jsonify(errno=RET.NODATA, errmsg="参数不能为空")
    #操作类型校验
    if not action in ["follow", "unfollow"]:
        return jsonify(errno=RET.DATAERR, errmsg = "操作类型错误")
    #根据操作类型是关注还是取消关注,关注就加到作者关注列表中,取消相反
    author = User.query.get(author_id)
    if action == "follow":
        if not g.user in author.followers:
            author.followers.append(g.user)
    else:
        if g.user in author.followers:
            author.followers.remove(g.user)


    #保存数据

    #返回数据
    return jsonify(errno=RET.OK, errmsg = "操作成功")
#点赞功能实现
@news_blue.route("/comment_like", methods=["POST"])
@user_login_data
def comment_like():
    #判断用户是否登录
    if not g.user:
        return jsonify(errno = RET.DBERR, errmsg = "用户没用登录")
    #获取参数
    comment_id = request.json.get("comment_id")
    action = request.json.get("action")

    #为空检验
    if not all([comment_id,action]):
        return jsonify(errno = RET.DBERR, errmsg = "参数不全")
    #操作类型校验
    if not action in ["add", "remove"]:
        return jsonify(errno = RET.DATAERR, errmsg = "操作类型有误")
    #根据评论编号取出评论对象，判断是否存在
    comment = Comment.query.filter().get(comment_id)
    if not comment: return jsonify(errno=RET.NODATA, errmsg = "评论不存在")
    #根据操作类型，点赞或者取消点赞
    if action == "add":
        comment_like = CommentLike.query.filter(CommentLike.comment_id == comment_id, CommentLike.user_id ==g.user.id).first()
        if not comment_like:
            #创建点赞对象设置属性
            comment_like = CommentLike()
            comment_like.comment_id = comment_id
            comment_like.user_id = g.user.id

            #增加数据库
            db.session.add(comment_like)
            db.session.commit()
            #将评论的点赞数量加1
            comment.like_count += 1
    else:
        comment_like = CommentLike.query.filter(CommentLike.comment_id == comment_id,CommentLike.user_id == g.user.id).first()
        if comment_like:
            # 删除点赞对象
            db.session.delete(comment_like)

            # 增加数据库
            db.session.commit()
            # 将评论的点赞数量加1
            if comment.like_count > 0:
                comment.like_count -= 1
    #返回响应
    return jsonify(errno = RET.OK, errmsg = "操作成功")











# 评论功能实现
@news_blue.route("/news_comment", methods=["POST"])
@user_login_data
def news_comment():
    # 判断用户有没有登录
    if not g.user:
        return jsonify(errno=RET.NODATA, errmsg="用户没有登录")
    # 获取参数
    news_id = request.json.get("news_id")
    content = request.json.get("comment")
    parent_id = request.json.get("parent_id")
    # 为空校验
    if not all([news_id, content]):
        return jsonify(errno=RET.NODATA, errmsg="空参数")
    # 获取新闻
    try:
        news = News.query.filter().get(news_id)
    except Exception as e:
        current_app.logger.error(e)

    if not news:
        return jsonify(errno=RET.THIRDERR, errmsg="新闻不存在")
    # 创建一个评论类对象

    comment_object = Comment()

    comment_object.user_id = g.user.id
    comment_object.news_id = news_id
    comment_object.content = content

    if parent_id:
        comment_object.parent_id = parent_id
    try:
        db.session.add(comment_object)
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg="评论失败")

    return jsonify(errno=RET.OK, errmsg="评论成功", data=comment_object.to_dict())


# 收藏功能
@news_blue.route("/news_collect", methods=["post"])
@user_login_data
def news_collect():
    # 先判断用户是否登录，这是一个大前提
    if not g.user:
        return jsonify(errno=RET.NODATA, errmsg="用户没用登录")
    # 获取参数
    # news_id = request.json.get("news_id")

    # action = request.json.get("action")
    data_dict = request.json
    news_id = data_dict.get("news_id")
    action = data_dict.get("action")

    # 为空校验
    if not all([news_id, action]):
        return jsonify(errno=RET.DBERR, errmsg="参数不能为空")
    # 判断操作类型是否正确
    if not action in ["collect", "cancel_collect"]:
        return jsonify(errno=RET.DBERR, errmsg="操作类型错误")
    # 取出新闻对象(数据)
    try:
        news = News.query.filter().get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.THIRDERR, errmsg="获取新闻失败")
    if not news:
        return jsonify(errno=RET.NODATA, errmsg="新闻不存在")
    # 根据操作类型进行具体操作
    try:
        if action == "collect":
            if not news in g.user.collection_news:
                g.user.collection_news.append(news)
        else:
            if news in g.user.collection_news:
                g.user.collection_news.remove(news)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="操作失败")

    # 操作成功返回数据
    return jsonify(errno=RET.OK, errmsg="操作成功")


@news_blue.route("/<int:news_id>")
@user_login_data
def news_info(news_id):
    # 根据新闻编号查询新闻对象
    news = News.query.filter().get(news_id)
    # 判断新闻对象是否存在
    if not news:
        abort(404)
    try:
        newss = News.query.filter().order_by(News.clicks).limit(6).all()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="新闻获取失败")

    click_news_list = []
    for new in newss:
        click_news_list.append(new.to_dict())

    # 获取用户
    # userid = session.get("user_id")
    # user = User.query.filter().get(userid)

    # 　判断新闻
    is_collected = False
    if g.user and news in g.user.collection_news:
        is_collected = True

    # 查询所有评论对象
    try:
        comments = Comment.query.filter(Comment.news_id == news_id).order_by(Comment.create_time.desc()).all()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="获取评论失败")

    # 找出用户所有点过的赞
    commentlikes = []
    try:
        if g.user:
            commentlikes = CommentLike.query.filter(CommentLike.user_id == g.user.id).all()
    except Exception as e:
        current_app.logger.error(e)

    # 获取用户点过赞的评论编号列表
    comment_like_ids = []
    for commentlike in commentlikes:
        comment_like_ids.append(commentlike.comment_id)

    # 将评论对象列表转换成字典列表
    comment_list = []
    for comment in comments:

        comm_dict = comment.to_dict()
        # 设置点赞key
        comm_dict["is_like"] = False
        # 判断用户是否对该评论点过赞
        if g.user and comment.id in comment_like_ids:
            comm_dict["is_like"] = True

        comment_list.append(comm_dict)
    is_followed= False
    # is_followed= True
    if g.user and news.user: #这个新问有作者
        if g.user in news.user.followers:
            is_followed = True





    # 拼接数据渲染页面
    data = {

        "news_info": news.to_dict(),
        "click_news_list": click_news_list,
        "user_info": g.user.to_dict() if g.user else "",
        "is_collected": is_collected,
        "comments": comment_list,
        "is_followed": is_followed
    }

    return render_template("news/detail.html", data=data)
