from flask import g, jsonify, render_template, redirect, request, current_app

from info import db
from utils import constants
from utils.commons import user_login_data
from utils.constants import QINIU_DOMIN_PREFIX
from utils.image_storage import image_storage
from utils.models import User, News, Category
from utils.response_code import RET
from . import profile_blue
from werkzeug.security import generate_password_hash, check_password_hash


@profile_blue.route("/user_follow")
@user_login_data
def user_follow():
    # 获取参数
    page = request.args.get("p", "1")
    # 参数类型转换
    page = int(page)
    # 查询关注的作者
    try:
        authors = g.user.followed.order_by(User.create_time.desc()).paginate(page, 2, False)
        # authors = g.user.followed.paginate(page, 2, False)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg = "获取关注作者失败")
    total_page = authors.pages
    authors_ob = authors.items
    currentPage = authors.pages
    author_list = []
    for author in authors_ob:
        author_list.append(author.to_dict())
    # data
    data = {
        "author_list": author_list,
        "total_page": total_page,
        "currentPage": currentPage
    }

    # 返回数据

    return render_template("news/user_follow.html", data=data)


# 新闻列表
@profile_blue.route("/news_list")
@user_login_data
def news_list_func():
    # 获取参数
    p = request.args.get("p", "1")
    # 校验参数，类型转换
    if not p:
        return jsonify(errno=RET.NODATA, errmsg="参数不能为空")
    p = int(p)
    # 取出用户的新闻对象，取出相应的属性
    news = News.query.filter(News.user_id == g.user.id).order_by(News.create_time.desc()).paginate(p, 2, False)
    total_page = news.pages
    current_page = news.page
    items = news.items

    news_list = []
    for item in items:
        news_list.append(item.to_review_dict())

    # data
    # data = {
    #     "total_page": total_page,
    #     "current_page": current_page,
    #     "news_list": news_list
    #
    # }
    data = {
        "totalPage": total_page,
        "currentPage": current_page,
        "news": news_list

    }
    # 返回数据
    return render_template("news/user_news_list.html", data=data)


# 发布新闻
@profile_blue.route("/news_release", methods=["GET", "POST"])
@user_login_data
def news_release():
    # 判断用户是否登录
    if not g.user:
        return jsonify(errno=RET.NODATA, errmsg="用户没有登录")
    # 判断请求方法
    if request.method == "GET":
        categories = Category.query.all()
        categories_list = []
        for category in categories:
            categories_list.append(category.to_dict())

        # data = {
        #     "categories_list":categories_list
        # }
        return render_template("news/user_news_release.html", categories=categories_list)

    # 获取参数
    # print(request)
    title = request.form.get("title")
    category_id = request.form.get("category_id")
    digest = request.form.get("digest")
    index_image = request.files.get("index_image")
    content = request.form.get("content")
    # 为空校验
    if not all([title, category_id, digest, index_image, content]):
        return jsonify(errno=RET.NODATA, errmsg="参数不能为空")
    try:
        image_name = image_storage(index_image.read())
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.THIRDERR, errmsg="千牛云上传失败")
    if not image_name:
        return jsonify(errno=RET.NODATA, errmsg="上传失败")

    # 创建新闻对象对象并保存
    news = News()
    news.title = title
    news.source = g.user.nick_name
    news.user_id = g.user.id
    news.category_id = category_id
    news.digest = digest
    news.content = content
    news.status = 1
    news.index_image_url = constants.QINIU_DOMIN_PREFIX + image_name

    try:
        db.session.add(news)
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据储存失败")

    # 返回数据
    return jsonify(errno=RET.OK, errmsg="操作成功")


# 获取用户收藏列表
@profile_blue.route("/collection")
@user_login_data
def collection():
    # h获取参数
    # print(request)

    page = request.args.get("p", "1")
    # 参数类型转换
    page = int(page)
    # 分页查询
    paginate = g.user.collection_news.order_by(News.create_time.desc()).paginate(page, 2, False)
    # 取出分页对象属性，总页数，当前页，当前页对象列表
    totalPage = paginate.pages
    currentPage = paginate.page
    items_ob_li = paginate.items
    # 将对象列表转换成字典列表
    news_list = []
    for new in items_ob_li:
        news_list.append(new.to_dict())

    # data
    data = {
        "totalPage": totalPage,
        "currentPage": currentPage,
        "items_l": news_list

    }

    return render_template("news/user_collection.html", data=data)


# 上传图片
@profile_blue.route("/pic_info", methods=["GET", "POST"])
@user_login_data
def pic_info():
    # 判断请求的方法
    if request.method == "GET":
        return render_template("news/user_pic_info.html", user=g.user.to_dict())
    # 获取参数
    avatar = request.files.get("avatar")
    # 校验参数为空校验
    if not avatar:
        return jsonify(errno=RET.PARAMERR, errmsg="参数不全")

    # 上传图片并判断是否上传成功
    try:
        image_name = image_storage(avatar.read())
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.THIRDERR, errmsg="七牛云上传失败")

    # 判断上传后是否返回值
    if not image_name:
        return jsonify(errno=RET.NODATA, errmsg="上传失败")
    print(image_name)
    # 把图片数据保存到数据库
    g.user.avatar_url = image_name
    # 设置了师徒返回数据口自动提交
    # pwzo1thkv.bkt.clouddn.com

    data = {
        "avatar_url": constants.QINIU_DOMIN_PREFIX + image_name
    }
    return jsonify(errno=RET.OK, errmsg="上传成功", data=data)


@profile_blue.route("/pass_info", methods=["GET", "POST"])
@user_login_data
def pass_info():
    if request.method == "POST":
        # 获取参数
        old_password = request.json.get("old_password")
        new_password = request.json.get("new_password")
        # 校验密码
        if not check_password_hash(g.user.password_hash, old_password):
            return jsonify(errno=RET.DATAERR, errmsg="密码错误")
        # 储存新密码到数据库
        g.user.password_hash = generate_password_hash(new_password)

        return jsonify(errno=RET.OK, errmsg="修改密码成功")

    return render_template("news/user_pass_info.html")


@profile_blue.route("/base_info", methods=["get", "post"])
@user_login_data
def base_info():
    if request.method == "GET":
        return render_template("news/user_base_info.html", user=g.user.to_dict())

    # 获取参数
    nick_name = request.json.get("nick_name")
    signature = request.json.get("signature")
    gender = request.json.get("gender")

    # 校验参数
    if not all([nick_name, signature, gender]):
        return jsonify(errno=RET.NODATA, ermsg="参数为空")
    # 判断性别是否合理
    if not gender in ["MAN", "WOMAN"]:
        return jsonify(errno=RET.DATAERR, errmsg="性别数据有误")

    # 修改用户数据
    try:
        # user = User.query.filter().get(g.user.id)
        g.user.nick_name = nick_name
        g.user.signature = signature
        g.user.gender = gender
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.THIRDERR, errmsg="数据操作失败")
    # 返回相应
    return jsonify(errno=RET.OK, errmsg="操作成功")


@profile_blue.route("/info")
@user_login_data
def profile_user():
    # 判断用户是否登录
    if not g.user:
        return redirect("/")
    # 查询用户信息
    # user_login_data里封装了查询用户信息的过程所以这个一步省略

    # 构造data字典信息
    data = {
        "user_info": g.user.to_dict()
    }

    return render_template("news/user.html", data=data)
