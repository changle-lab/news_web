from flask import current_app, session, jsonify, request, g
from info import redis_store
# from info import client_m,
from utils import constants
from utils.commons import user_login_data
from utils.models import User, News, Category
from utils.response_code import RET
from . import index_blue
from flask import render_template
from info.templates.news import *


@index_blue.route("/newslist")
def newslist():
    # get请求，args
    cid = request.args.get("cid", "1")
    page = request.args.get("page", "1")
    per_page = request.args.get("per_page", "10")
    # 上面取出来的是字符串，但查询数据库是要用int类型，所以需要转换
    try:

        page = int(page)
        per_page = int(per_page)
    # 如果转换失败, 他们默认值赋予1
    except Exception as e:
        page = 1
        per_page = 1
    try:
        # 新闻没有最新，数据库中也没有cid是1的类型
        filter_condition = ""
        if cid != "1":
            filter_condition = (News.category_id == cid)

        # 查出cid这个累的新闻然后按照创建日期倒叙排序，显示指定页数
        paginates = News.query.filter(filter_condition).order_by(News.create_time.desc()).paginate(page,per_page,False)
        # paginates = News.query.filter(filter_condition).paginate(page, per_page, False)


    except Exception as e:
        current_app.logger.error(e)
        return jsonify(error=RET.DBERR, errmsg="获取新闻失败")
    # 返回内容，总页数，当前页面，当前新闻数据的分类编号，新闻列表数据
    pages = paginates.pages
    page = paginates.page
    items = paginates.items

    newslist = []
    for item in items:
        newslist.append(item.to_dict())

        # data = {
    #     "totalPage": pages,
    #     "correntPage": page,
    #     "cid":cid,
    #     "newsList":newslist,
    #
    # }

    return jsonify(errno=RET.OK, errmsg="获取新闻成功", totalPage=pages, correntPage=page, cid=cid, newsList=newslist)


# 右上角展示区域
@index_blue.route("/", methods=['get', "post"])
@user_login_data
def show_index():
    # 取出session中用户编号
    # user_id = session.get("user_id")
    # # 根据user_id取出用户对象
    # user = None
    # if user_id:
    #     try:
    #         user = User.query.filter().get(user_id)
    #     except Exception as e:
    #         current_app.logger.error(e)
    # 查询热门新闻，前10条，按照点击量排序
    try:
        news = News.query.filter().order_by(News.clicks.desc()).limit(constants.CLICK_RANK_MAX_NEWS).all()
        news_a = News.query.filter().count()
        # print("新闻总数据个数　：%s" % news_a)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="获取热门新闻失败")
    print("查询出来的元数据：", news)
    # 列表装换成字典
    click_news_list = []
    for item in news:
        click_news_list.append(item.to_dict())
    # 查询所有分类
    categories = Category.query.all()

    category_list = []

    for category in categories:
        category_list.append(category.to_dict())

    # 携带用户数据渲染页面
    data = {

        "user_info": g.user.to_dict() if g.user else "",
        "click_news_list": click_news_list,
        "category_list": category_list

    }

    return render_template("news/index.html", data=data)


@index_blue.route("/favicon.ico")
def favicon():
    # 这个是返回静态文件，render_template
    return current_app.send_static_file("news/favicon.ico")
