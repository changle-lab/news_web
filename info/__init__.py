# -*- coding:UTF-8 -*-
from utils.commons import user_login_data
import logging
from logging.handlers import RotatingFileHandler

from flask_session import Session
from flask_sqlalchemy import SQLAlchemy
import redis
from flask import Flask, session, render_template, g
from flask_wtf.csrf import CSRFProtect, generate_csrf
from config import config_dict

redis_store = None
db = SQLAlchemy()


def create_app(config_name):
    app = Flask(__name__)

    config_3 = config_dict.get(config_name)
    levelname = config_3.LEVELNAME
    log_file(levelname)

    app.config.from_object(config_3)

    db.init_app(app)
    global redis_store
    redis_store = redis.StrictRedis(host=config_3.REDIS_HOST, port=config_3.REDIS_PORT, decode_responses=True)

    # 读取app身上的session信息
    Session(app)
    # CSRF 保护app
    CSRFProtect(app)

    from info.modules.index import index_blue
    from info.modules.passport import passport_blue
    from modules.news import news_blue

    app.register_blueprint(index_blue)
    app.register_blueprint(passport_blue)
    # 注册新闻蓝图
    app.register_blueprint(news_blue)
    # 注册个人中心蓝图
    from modules.profile import profile_blue
    app.register_blueprint(profile_blue)

    # 将函数添加到过滤器列表
    from utils.commons import index_class
    app.add_template_filter(index_class, "index_class")

    # 监听404异常的产生，返回一个完美的错误页面
    @app.errorhandler(404)
    @user_login_data
    def page_not_found(e):
        data = {
            "user_info": g.user.to_dict() if g.user else ""
        }
        return render_template("news/404.html", data=data)

    # 加上请求钩子，拦截所有请求，对请求数据设置csrf_token统一返回
    @app.after_request
    def after_request(resp):
        # 设置csrf_token

        csrf_token = generate_csrf()

        resp.set_cookie("csrf_token", csrf_token)

        return resp

    # print(app.url_map)
    return app




# 记录日志信息

def log_file(levelname):
    # 设置日志的记录等级
    logging.basicConfig(level=levelname)  # 调试debug级
    # 创建日志记录器，指明日志保存的路径、每个日志文件的最大大小、保存的日志文件个数上限
    file_log_handler = RotatingFileHandler("logs/log", maxBytes=1024 * 1024 * 100, backupCount=10)
    # 创建日志记录的格式 日志等级 输入日志信息的文件名 行数 日志信息
    formatter = logging.Formatter('%(levelname)s %(filename)s:%(lineno)d %(message)s')
    # 为刚创建的日志记录器设置日志记录格式
    file_log_handler.setFormatter(formatter)
    # 为全局的日志工具对象（flask app使用的）添加日志记录器
    logging.getLogger().addHandler(file_log_handler)
