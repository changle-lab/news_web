# 储存公用代码

# 使用过滤器，过滤颜色提示
from functools import wraps

from flask import session, current_app, g



def index_class(index):
    if index == 1:
        return "first"
    elif index ==2:
        return "second"
    elif index ==3:
        return "third"
    else:
        return ""

#封装登录数据
def user_login_data(view_func):
    @wraps(view_func)
    def wrapper(*args, **kwargs):
        from utils.models import User
        user_id = session.get("user_id")

        user = None
        if user_id:
            try:
                user = User.query.filter().get(user_id)
            except Exception as e:
                current_app.logger.error(e)
        #封装到G对象身上
        g.user = user

        return view_func(*args, **kwargs)
    return wrapper

