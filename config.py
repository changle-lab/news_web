from redis import StrictRedis
from datetime import timedelta
import logging

# 配置类
class Config(object):
    DEBUG = True
    SECRET_KEY = "feuifejf3jnr3r83fn4jri43t84fhu4ht"

    REDIS_HOST = "127.0.0.1"
    REDIS_PORT = 6379

    SQLALCHEMY_DATABASE_URI = "mysql://root:mysql@localhost:3306/news_db"
    SQLALCHEMY_TRACK_MODIFICATIONS= False
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True #视图函数结束，自动提交一次回话

    SESSION_TYPE = "redis"
    SESSION_REDIS = StrictRedis(host=REDIS_HOST,port=REDIS_PORT)
    SESSION_USE_SIGNER = True
    PERMANENT_SESSION_LIFETIME = timedelta(days=2)

    #默认的日志级别
    LEVELNAME = logging.DEBUG

#开发模式下
class DevelopConfig(Config):
    pass

#上线模式
class ProductConfig(Config):
    DEBUG = False
    LEVELNAME = logging.ERROR

#测试模式配置信息
class TestingConfig(Config):
    TESTING = True

#提供配置类的统一入口
config_dict = {
    "develop":DevelopConfig,
    "product":ProductConfig,
    "testing":TestingConfig,
}

